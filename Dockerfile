# #Rather than scratch, we can use a prebuilt image with JDK already installed

#Simple copying over Jar files to container
#-----------------------------------------------------------------
FROM openjdk:21-ea-17-slim-buster

#copying from host file path --> pasting to new file path 
COPY target/*.jar /a/demo.jar

CMD ["java", "-jar", "/a/demo.jar"]

# # To run in detached mode and to access at port 8007 in a host machine. 
# #docker run -d -p 8007:8007  planet-api-example 
#-----------------------------------------------------------------


#Single Stage Dockerfile 
#-----------------------------------------------------------------
# FROM maven:3.9.2-amazoncorretto-17 
# COPY ./ ./ 
# RUN mvn clean package
# CMD ["java", "-jar", "/PlanetsAPI-0.0.1-SNAPSHOT.jar"]
#-----------------------------------------------------------------

#Mutli stage Dockerfile 
#-----------------------------------------------------------------
# #Build our Jar file 
# FROM maven:3.9.2-amazoncorretto-17 AS Maven_Build_Stage

# #Copy all our source files
# COPY ./ ./ 

# #Create a Jar file 
# RUN mvn clean package

# #Create a lightweight distributable image
# FROM openjdk:21-ea-17-slim-buster

# COPY --from=Maven_Build_Stage /target/*.jar /demo.jar

# CMD ["java", "-jar", "/demo.jar"]
#-----------------------------------------------------------------